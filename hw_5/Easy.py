import aiohttp
import asyncio
import os
import aiofiles


async def download_images(num_images: int, save_dir: str):
    """
    Downloads a specified number of images from https://picsum.photos, and
    saves them to a specified directory.

    Args:
        num_images (int): The number of images to download.
        save_dir (str): The directory to save the downloaded images to.

    Returns:
        None: This function doesn't return anything, but it will produce a
        print statement indicating that the images have been successfully
        downloaded.
    """

    os.makedirs(save_dir, exist_ok=True)
    async with aiohttp.ClientSession() as session:
        for i in range(num_images):
            async with session.get("https://picsum.photos/1000") as response:
                file_name = os.path.join(save_dir, f"{i}.jpg")
                async with aiofiles.open(file_name, "wb") as f:
                    await f.write(await response.read())
    print(f"Successfully downloaded {num_images} images")


if __name__ == "__main__":
    asyncio.run(download_images(10, "hw_5/images"))
