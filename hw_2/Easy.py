# from hw_2.img_generator_library.src.img_generator import generate_image
from utils import generate_header, generate_footer, save_latex


def generate_table(arr):
    return "\\begin{{tabular}} {{{}}} {} \\end{{tabular}}".format(
        "|c" * max([len(i) for i in arr]) + "|",
        " \\hline "
        + " \\\\ \\hline ".join([" & ".join(map(str, i)) for i in arr])
        + " \\\\ \\hline ",
    )


def generate_latex_with_table(arr):
    return (
        f"{generate_header()} \n {generate_table(arr)} \n {generate_footer()}"
    )


if __name__ == "__main__":
    arr1 = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    save_latex(generate_latex_with_table(arr1), "./artifacts/latex1.tex")

    arr2 = [[1, 2], [4, 5], [7, 8]]
    save_latex(generate_latex_with_table(arr2), "./artifacts/latex2.tex")

    arr3 = [[1, 2, 3], [4, 5, 6]]
    save_latex(generate_latex_with_table(arr3), "./artifacts/latex3.tex")
