def generate_header():
    return """\\documentclass{article}
\\usepackage{graphicx}
\\usepackage[utf8]{inputenc}
\\usepackage[english]{babel}
\\title{AdvancedPython}
\\author{NiKoV}
\\date{March 2023}
\\begin{document}
\\textbf{{\\LARGE Ovchinniov Nikita. HW2}} \\newline"""


def generate_footer():
    return "\\end{document}"


def save_latex(latex_string, filename="./hw_2/artifacts/latex.tex"):
    with open(filename, "w") as file:
        file.write(latex_string)
