from utils import generate_header, generate_footer, save_latex
from ast_visualizer_NiKoV.visualize import generate_graph
from Easy import generate_table


def generate_latex_with_table_and_img(table_arr, img_filename):
    return (
        f"{generate_header()} \n {generate_table(table_arr)} \n \\newline"
        f"{add_image(img_filename)} \n {generate_footer()}"
    )


def add_image(filename="./hw_2/artifacts/img"):
    generate_graph("./fib.py", filename)
    return f"\\includegraphics[width=15cm]{{{filename.split('/')[-1]}.png}}"
    # return f"\\includegraphics[width=15cm]{{{filename}}}.png"


if __name__ == "__main__":
    arr = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

    save_latex(
        generate_latex_with_table_and_img(arr, "./artifacts/img"),
        "./artifacts/medium_latex4.tex",
    )
