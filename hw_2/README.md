# Лабораторная работа №2

Изначально репозиторий уже содержит необходимые артефакты для сдачи ДЗ. Для того чтобы проверить то что артефакты генерируются именно кодом, нужно удалить все файлы из папки artifacts и запустить docker-compose, командой "docker-compose up".

## Easy
1. Файлы latex1.tex, latex2.tex, latex3.tex - это примеры генерации latex таблиц из двумерного массива.

## Medium
1. img.png - Картинка, сгенерированная лабораторной работой №1.

1. Ссылка на репозиторий для генерации картинки: https://test.pypi.org/project/ast-visualizer-NiKoV/#description

1. medium_latex4.tex - полученный tex файл
1. medium_latex4.pdf - полученный pdf файл

## Hard
1. Запустить "docker-compose up"
