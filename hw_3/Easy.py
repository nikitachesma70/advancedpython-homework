import numpy as np
import random

random.seed(0)
np.random.seed(0)


class Matrix:
    def __init__(self, grid) -> None:
        self.grid = grid
        self.rows = len(grid)
        self.cols = len(grid[0])

    def __repr__(self) -> str:
        matrix_str = f"{self.__class__.__name__} ["
        for row in self.grid:
            matrix_str += f"{row}"
        matrix_str += "]"
        return matrix_str

    def __str__(self) -> str:
        matrix_str = ""
        for row in self.grid:
            matrix_str += f"{row}\n"
        return matrix_str

    def __getitem__(self, index):
        return self.grid[index]

    def __setitem__(self, index, value):
        self.grid[index] = value

    def __add__(self, other):
        if not isinstance(other, (Matrix, int, float)):
            raise ValueError("Can only add matrix or scalars")
        if isinstance(other, Matrix):
            if self.rows != other.rows or self.cols != other.cols:
                raise ValueError(
                    "Matrices must have the same dimensions to be added."
                )
            result = self.__class__(
                [[0 for _ in range(self.cols)] for _ in range(self.rows)]
            )
            for i in range(self.rows):
                for j in range(self.cols):
                    result[i][j] = self[i][j] + other[i][j]
            return result
        else:
            result = self.__class__(
                [[0 for _ in range(self.cols)] for _ in range(self.rows)]
            )
            for i in range(self.rows):
                for j in range(self.cols):
                    result[i][j] = self[i][j] + other
            return result

    def __mul__(self, other):
        if isinstance(other, (int, float)):
            result = self.__class__(
                [[0 for _ in range(self.cols)] for _ in range(self.rows)]
            )
            for i in range(self.rows):
                for j in range(self.cols):
                    result[i][j] = self[i][j] * other
            return result
        elif isinstance(other, Matrix):
            if self.rows != other.rows or self.cols != other.cols:
                raise ValueError(
                    "Matrices must have the same dimensions for element-wise \
                        multiplication."
                )
            result = self.__class__(
                [[0 for _ in range(self.cols)] for _ in range(self.rows)]
            )
            for i in range(self.rows):
                for j in range(self.cols):
                    result[i][j] = self[i][j] * other[i][j]
            return result
        else:
            raise ValueError(
                "Can only element-wise multiplication matrix \
                or scalars"
            )

    def __matmul__(self, other):
        if isinstance(other, (int, float)):
            result = self.__class__(
                [[0 for _ in range(self.cols)] for _ in range(self.rows)]
            )
            for i in range(self.rows):
                for j in range(self.cols):
                    result[i][j] = self[i][j] * other
            return result
        elif isinstance(other, Matrix):
            if self.cols != other.rows:
                raise ValueError(
                    "Number of columns in the first matrix must match the \
                        number of rows in the second matrix."
                )
            result = self.__class__(
                [[0 for _ in range(other.cols)] for _ in range(self.rows)]
            )
            for i in range(self.rows):
                for j in range(other.cols):
                    sum = 0
                    for k in range(self.cols):
                        sum += self[i][k] * other[k][j]
                    result[i][j] = sum
            return result
        else:
            raise ValueError(
                "Can only multiply matrices by scalar or matrix of compatible \
                    dimensions."
            )


a = Matrix(np.random.randint(0, 10, (10, 10)))
b = Matrix(np.random.randint(0, 10, (10, 10)))


def write_operation_in_file(filename, result):
    with open(filename, "w") as file:
        file.write(str(result))


if __name__ == "__main__":
    write_operation_in_file("./hw_3/artifacts/easy/matrix+.txt", a + b)
    write_operation_in_file("./hw_3/artifacts/easy/matrix_mul.txt", a * b)
    write_operation_in_file("./hw_3/artifacts/easy/matrix@.txt", a @ b)
    # print(a)
    # print(b)
    # print(a + b)
    # print(a + 5)
    # print(a @ b)
    # print(a * b)
    # print(a * 5)
