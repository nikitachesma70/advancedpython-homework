from Easy import Matrix, write_operation_in_file
import numpy as np

# Хеш реализован довольно просто, находим определитель матрицы и применяем
# целочисленное деление на сумму всех элементов


class MatrixWithHash(Matrix):
    def __hash__(self) -> int:
        return int(np.linalg.det(self.grid)) * sum(
            [sum(row) for row in self.grid]
        )


if __name__ == "__main__":
    # Коллизии у хеша: самый простой пример, не менять числа,
    # а переставить их так, чтобы определитель от этого не поменялся, например
    # транспонировать матрицу или другими способами, поэтому программу
    # для поиска коллизий писать не обязательно

    a = MatrixWithHash(([1, 2], [3, 4]))
    b = MatrixWithHash(([5, 6], [7, 8]))
    c = MatrixWithHash(([4, 3], [2, 1]))
    d = MatrixWithHash(([5, 6], [7, 8]))

    write_operation_in_file("./hw_3/artifacts/hard/A.txt", a)
    write_operation_in_file("./hw_3/artifacts/hard/B.txt", b)
    write_operation_in_file("./hw_3/artifacts/hard/C.txt", c)
    write_operation_in_file("./hw_3/artifacts/hard/D.txt", d)
    write_operation_in_file("./hw_3/artifacts/hard/AB.txt", a @ b)
    write_operation_in_file("./hw_3/artifacts/hard/CD.txt", c @ d)
    write_operation_in_file("./hw_3/artifacts/hard/hash.txt", c @ d)
    write_operation_in_file(
        "./hw_3/artifacts/hard/hash.txt",
        f"AB_hash: {(a @ b).__hash__()} \nCD_hash:{(c @ d).__hash__()}",
    )

    # print(a.__hash__())
    # print(c.__hash__())
    # print(a @ b)
    # print(c @ d)
    # e = a @ b
    # f = c @ d
    # print(e.__hash__())
    # print(f.__hash__())
