import numpy as np
import random

random.seed(0)
np.random.seed(0)


class WriteInFile:
    def __write__(self, filename):
        with open(filename, "w") as file:
            file.write(str(self.grid))


class BeautyStr:
    def __str__(self):
        matrix_str = ""
        for row in self.grid:
            matrix_str += f"{row}\n"
        return matrix_str


class Setter:
    def __setitem__(self, index, value):
        self.grid[index] = value


class Getter:
    def __getitem__(self, index):
        return self.grid[index]


class Grid:
    def __init__(self, grid):
        super().__init__()
        self.grid = grid
        self.rows = len(grid)
        self.cols = len(grid[0])


class ArrayLike(
    Grid,
    Setter,
    Getter,
    np.lib.mixins.NDArrayOperatorsMixin,
    BeautyStr,
    WriteInFile,
):
    _HANDLED_TYPES = (list, int)

    def __array_ufunc__(self, ufunc, method, *inputs, **kwargs):
        out = kwargs.get("out", ())
        for x in inputs + out:
            if not isinstance(x, self._HANDLED_TYPES + (ArrayLike,)):
                return NotImplemented
        inputs = tuple(
            x.grid if isinstance(x, ArrayLike) else x for x in inputs
        )
        if out:
            kwargs["out"] = tuple(
                x.grid if isinstance(x, ArrayLike) else x for x in out
            )
        result = getattr(ufunc, method)(*inputs, **kwargs)
        if type(result) is tuple:
            return tuple(type(self)(x) for x in result)
        elif method == "at":
            return None
        else:
            return type(self)(result)


a = ArrayLike(np.random.randint(0, 10, (10, 10)))
b = ArrayLike(np.random.randint(0, 10, (10, 10)))

(a + b).__write__("./hw_3/artifacts/medium/matrix+.txt")
(a * b).__write__("./hw_3/artifacts/medium/matrix_mul.txt")
(a @ b).__write__("./hw_3/artifacts/medium/matrix@.txt")
