# В качестве бейзлайна взял готовое решение по визуализации через graphviz
# библиотеку
# https://github.com/pombredanne/python-ast-visualizer/blob/master/astvisualizer.py
# изначально написал визуализацию на networkx, но не хватило функционала,
# поэтому искал ещё решения


import ast
import graphviz as gv
import numbers
import re
from uuid import uuid4 as uuid


ignore_node = [
    "node_type",
    "type_ignores",
    "defaults",
    "decorator_list",
    "vararg",
    "type_comment",
    "kw_defaults",
    "kwarg",
    "returns",
]


def generate_graph(input_filename, output_file):

    with open(input_filename) as instream:
        code = instream.read()

    code_ast = ast.parse(code)
    transformed_ast = transform_ast(code_ast)

    renderer = GraphRenderer()
    renderer.render(transformed_ast, label="test", output_file=output_file)


def transform_ast(code_ast):
    if type(code_ast) in ignore_node:
        return
    elif isinstance(code_ast, ast.Constant):
        return code_ast.value
    elif isinstance(code_ast, ast.Name):
        return f"ARG:{code_ast.id}"
    elif isinstance(code_ast, ast.arg):
        return f"ARG:{code_ast.arg}"
    elif isinstance(code_ast, ast.AST):
        node = {
            f"{to_camelcase(k)}": transform_ast(getattr(code_ast, k))
            for k in code_ast._fields
        }
        node["node_type"] = to_camelcase(code_ast.__class__.__name__)
        return node
    elif isinstance(code_ast, list):
        return [transform_ast(el) for el in code_ast]
    else:
        return code_ast


def to_camelcase(string):
    return re.sub("([a-z0-9])([A-Z])", r"\1_\2", string).lower()


class GraphRenderer:
    """
    this class is capable of rendering data structures consisting of
    dicts and lists as a graph using graphviz
    """

    graphattrs = {
        "labelloc": "t",
        "fontcolor": "white",
        "bgcolor": "#333333",
        "margin": "0",
    }

    nodeattrs = {
        "color": "white",
        "fontcolor": "white",
        "style": "filled",
        "fillcolor": "#006699",
    }

    edgeattrs = {
        "color": "white",
        "fontcolor": "white",
    }

    color_dict = {
        "function_def": "#5bb967",
        "const": "#FF0000",
        "var": "#ec821f",
        "bin_op_all": "#F000FF",
        "compare_all": "#0F00F0",
        "aug_assign": "#d4779e",
        "assign": "#ee4e4e",
        "arguments": "#9b189b",
        "default": "#006699",
    }

    _graph = None
    _rendered_nodes = None
    _rendered_nodes_set = None

    @staticmethod
    def _escape_dot_label(str):
        return (
            str.replace("\\", "\\\\")
            .replace("|", "\\|")
            .replace("<", "\\<")
            .replace(">", "\\>")
        )

    def get_color_node(self, type_node):
        if type_node in self.color_dict:
            return self.color_dict[type_node]
        elif type_node in ["sub", "div", "mult", "sum", "bin_op"]:
            return self.color_dict["bin_op_all"]
        elif type_node in ["compare", "eq", "gt", "lt", "in", "not_in"]:
            return self.color_dict["compare_all"]
        return self.color_dict["default"]

    def _render_node(self, node):
        if isinstance(node, (str, numbers.Number)) or node is None:
            node_id = uuid()
        else:
            node_id = id(node)
        node_id = str(node_id)

        if node_id not in self._rendered_nodes:
            self._rendered_nodes.add(node_id)
            if isinstance(node, dict):
                self._render_dict(node, node_id)
            elif isinstance(node, list):
                if len(node) == 1:
                    node_id = self._render_node(node[0])
                else:
                    self._render_list(node, node_id)
            elif isinstance(node, int):
                self._graph.node(
                    node_id,
                    label=self._escape_dot_label(str(node)),
                    fillcolor=self.get_color_node("const"),
                )
            elif "ARG:" in str(node):
                node = str(node)[4:]
                self._graph.node(
                    node_id,
                    label=self._escape_dot_label(node),
                    fillcolor=self.get_color_node("var"),
                )
            else:
                self._graph.node(
                    node_id,
                    label=self._escape_dot_label(str(node)),
                    fillcolor=self.get_color_node("name"),
                )
        return node_id

    def _render_dict(self, node, node_id):

        node_type = node.get("node_type", "[dict]")
        self._graph.node(
            node_id, label=node_type, fillcolor=self.get_color_node(node_type)
        )
        for key, value in node.items():
            if key.split(":")[-1] in ignore_node:
                continue
            child_node_id = self._render_node(value)
            if child_node_id not in self._rendered_nodes_set:
                self._graph.edge(
                    node_id,
                    child_node_id,
                    label=self._escape_dot_label(key.split(":")[-1]),
                )

    def _render_list(self, node, node_id):
        if len(node) == 0:
            self._rendered_nodes_set.add(node_id)
            return
        self._graph.node(
            node_id,
            label="[list]",
        )
        for idx, value in enumerate(node):
            child_node_id = self._render_node(value)
            self._graph.edge(
                node_id,
                child_node_id,
                label=self._escape_dot_label(str(idx).split(":")[-1]),
            )

    def render(self, data, *, label=None, output_file="img"):
        # create the graph
        graphattrs = self.graphattrs.copy()
        if label is not None:
            graphattrs["label"] = self._escape_dot_label(label)
        graph = gv.Digraph(
            graph_attr=graphattrs,
            node_attr=self.nodeattrs,
            edge_attr=self.edgeattrs,
        )

        # recursively draw all the nodes and edges
        self._graph = graph
        self._rendered_nodes = set()
        self._rendered_nodes_set = set()
        self._render_node(data)
        self._graph = None
        self._rendered_nodes = None

        # display the graph
        graph.format = "png"
        # graph.view(
        #     directory="./hw_1/artifacts",
        #     filename="Hard_my",
        # )

        graph.render(
            directory="/".join(output_file.split("/")[:-1]),  # Take only path
            filename=output_file.split("/")[-1],  # Take only filename
        )


if __name__ == "__main__":
    generate_graph("./hw_1/Easy.py", "./hw_1/artifacts/img")
