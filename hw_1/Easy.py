# Для более красивой визуализации в задаче Hard, написал различные реализации
def fib_rec(n):
    if n in [0, 1]:
        return 1
    else:
        return n * fib_rec(n - 1)


def fib_for(n):
    result = 1
    for i in range(1, n + 1):
        result *= i
    return result


def fib_while(n):
    result = 1
    while n > 1:
        result *= n
        n -= 1
    return result


if __name__ == "__main__":
    print(f"fib_rec:{fib_rec(6)}")
    print(f"fib_for:{fib_for(6)}")
    print(f"fib_while:{fib_while(6)}")
