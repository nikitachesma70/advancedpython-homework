import concurrent.futures
import math
import time


def integrate_thread(f, a, b, *, jobs=1, n_iter=1000):
    step = (b - a) / n_iter
    acc = 0
    with concurrent.futures.ThreadPoolExecutor(max_workers=jobs) as executor:
        futures = []
        for i in range(n_iter):
            futures.append(executor.submit(f, a + i * step))
            # acc += f(a + i * step) * step
        for future in concurrent.futures.as_completed(futures):
            acc += future.result() * step
    return acc


def integrate_process(f, a, b, *, jobs=1, n_iter=1000):
    step = (b - a) / n_iter
    acc = 0
    with concurrent.futures.ProcessPoolExecutor(max_workers=jobs) as executor:
        futures = []
        for i in range(n_iter):
            futures.append(executor.submit(f, a + i * step))
        for future in concurrent.futures.as_completed(futures):
            acc += future.result() * step
    return acc


def stress_test(result_filename, func, a, b, func_in, count_process_thread):
    with open(result_filename, "w") as file:
        for i in range(1, count_process_thread + 1):
            start_time = time.time()
            func(func_in, a, b, n_jobs=i, n_iter=1000)
            end_time = time.time()
            print(f"{i}:Success")
            file.write(
                f"Count_threads(processors): {i},   Start: {start_time},   "
                f"End: {end_time},  "
                f"Delta: {end_time-start_time} \n"
            )


if __name__ == "__main__":
    stress_test(
        "./hw_4/artifacts/result_ProcessPoolExecutor.txt",
        func=integrate_process,
        a=0,
        b=math.pi / 2,
        func_in=math.cos,
        count_process_thread=12,
    )

    stress_test(
        "./hw_4/artifacts/result_ThreadPoolExecutor.txt",
        func=integrate_thread,
        a=0,
        b=math.pi / 2,
        func_in=math.cos,
        count_process_thread=12,
    )
