from datetime import datetime
from multiprocessing import Process
from threading import Thread
import sys

sys.setrecursionlimit(10000)


def fib(n):
    result = 1
    for i in range(1, n + 1):
        result *= i
    return


def test_fib_multithreading(count_threads=10, num=1000000):
    for _ in range(count_threads):
        t = Thread(target=fib, args=[num])
        t.start()


def test_fib_multiprocessing(count_process=10, num=1000000):
    for _ in range(count_process):
        p = Process(target=fib, args=[num])
        p.start()


def stress_test(result_filename, count_run, func, n, count_process_thread):
    with open(result_filename, "w") as file:
        for i in range(count_run):
            start_time = datetime.now()
            func(count_process_thread, n)
            end_time = datetime.now()
            print(f"{i}:Success")
            file.write(
                f"[{i}]  Start: {start_time.strftime('%H:%M:%S:%f')},  "
                f"End: {end_time.strftime('%H:%M:%S:%f')},  "
                f"Delta: {end_time-start_time} \n"
            )


if __name__ == "__main__":

    # stress_test(
    #     result_filename="./hw_4/artifacts/result_multiprocessing.txt",
    #     count_run=10,
    #     func=test_fib_multiprocessing,
    #     n=150000,
    #     count_process_thread=10,
    # )

    # Запускать, комментируя верхнюю функцию или нижнюю

    stress_test(
        result_filename="./hw_4/artifacts/result_multithreading.txt",
        count_run=10,
        func=test_fib_multithreading,
        n=150000,
        count_process_thread=10,
    )
