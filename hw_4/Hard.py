import multiprocessing as mp
import time
from datetime import datetime
import codecs


def sender(q, conn):
    while True:
        s = q.get()
        if s:
            conn.send(s.lower())
            time.sleep(5)


def receiver(conn):
    while True:
        s = conn.recv()
        if s:
            print(
                f"[RECEIVE] {datetime.now().strftime('%H:%M:%S')} "
                f"{codecs.encode(s, 'rot_13')}"
            )


if __name__ == "__main__":
    mp.set_start_method("spawn")
    parent_conn, child_conn = mp.Pipe()
    queue = mp.Queue()
    A = mp.Process(target=sender, args=(queue, child_conn))
    B = mp.Process(target=receiver, args=(parent_conn,))
    A.start()
    B.start()

    while True:
        inp = input()
        print(f"[SEND] {datetime.now().strftime('%H:%M:%S')} {inp}")
        queue.put(inp)
